# Installation Steps

## Plugins

### Camera
```bash
$ ionic cordova plugin add cordova-plugin-camera
$ npm install --save @ionic-native/camera
```

### File

```bash
$ ionic cordova plugin add cordova-plugin-file
$ npm install --save @ionic-native/file
```

### File Path

```bash
$ ionic cordova plugin add cordova-plugin-filepath
$ npm install --save @ionic-native/file-path
```

### File Transfer 

```bash
$ ionic cordova plugin add cordova-plugin-file-transfer
$ npm install --save @ionic-native/file-transfer
```


## Platforms

### iOS
```bash
$ ionic cordova platform add ios 
```

### Android
```bash
$ ionic cordova platform add android 
```
